from celery_tasks.main import celery_app


# 异步任务实现
from . import constants
from .yuntongxun.sms import CCP


@celery_app.task(name='send_sms_code')
def send_sms_code(mobile,sms_codes):

    CCP().send_template_sms(mobile, [sms_codes, constants.SMS_CODE_REDIS_EXPIRE // 60], 1)