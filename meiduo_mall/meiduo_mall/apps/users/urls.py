from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework import routers

from .views import UserBrowsingHistoryView,UsernameCountView,MobileCountView,UserView,UserDetailView,EmailView,EmailVerifyView, AddressViewSet

urlpatterns = [
    url(r'^usernames/(?P<username>\w{5,20})/count/$',UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$',MobileCountView.as_view()),
    url(r'^users/$',UserView.as_view()),
    # login
    url(r'^authorizations/$',obtain_jwt_token),
    url(r'^user/$',UserDetailView.as_view()),
    url(r'^email/$',EmailView.as_view()),
    url(r'^emails/verification/$',EmailVerifyView.as_view()),
    url(r'^browse_histories/$',UserBrowsingHistoryView.as_view()),

]
router = routers.DefaultRouter()
router.register(r'addresses', AddressViewSet, base_name='addresses')

urlpatterns += router.urls

# POST /addresses/ 新建  -> create
# PUT /addresses/<pk>/ 修改  -> update
# GET /addresses/  查询  -> list
# DELETE /addresses/<pk>/  删除 -> destroy
# PUT /addresses/<pk>/status/ 设置默认 -> status
# PUT /addresses/<pk>/title/  设置标题 -> title
