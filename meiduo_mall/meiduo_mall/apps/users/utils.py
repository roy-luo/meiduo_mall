import re
from django.contrib.auth.backends import ModelBackend


from .models import User
def get_user_by_account(account):
    """根据手机号或者用户名获得用户"""
    try:
        if re.match(r'^1[3-9]\d{9}$',account):
            user = User.objects.get(mobile=account)
        else:
            user = User.objects.get(username=account)
    except User.DoesNotExist:
        return False
    else:
        return user










class UsernameMobileAuthBackend(ModelBackend):
    """为了增加手机号登录方式"""

    def authenticate(self, request, username=None, password=None, **kwargs):

        user = get_user_by_account(username)

        if user and user.check_password(password):
            return user







def jwt_response_payload_handler(token,user,request):
    """重写JWT响应数据的方法"""
    # 增加username和user_id
    return {
        'username':user.username,
        'user_id':user.id,
        'token':token
    }
