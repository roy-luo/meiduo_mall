import re

from rest_framework import serializers
from django_redis import get_redis_connection

from celery_tasks.email.tasks import send_verify_email
from goods.models import SKU
from .models import User
from rest_framework_jwt.settings import api_settings
from .models import Address


class UserBrowsingHistorySerializer(serializers.Serializer):

    # 新增校验字段
    sku_id = serializers.IntegerField(label='商品编号',min_value=1)

#     单独校验字段
    def validate_sku_id(self, value):
        try:
            sku = SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('获取不到该商品')
        return value


    def create(self, validated_data):

        #获取user_id
        user_id = self.context['request'].user.id

        # 获取sku_id商品编号
        sku_id = validated_data.get('sku_id')


        # 创建连接redis对象
        redis_conn = get_redis_connection('history')

        #管道
        pl = redis_conn.pipeline()

        #去重
        pl.lrem("history_%s" % user_id,0,sku_id)

        #添加
        pl.lpush("history_%s" % user_id, sku_id)

        #截取
        pl.ltrim('history_%s' % user_id,0,4)


        #执行操作

        pl.execute()


        return validated_data



class UserAddressSerializer(serializers.ModelSerializer):
    """
    用户地址序列化器
    """
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_time', 'update_time')

    def validate_mobile(self, value):
        """
        验证手机号
        """
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value



    def create(self, validated_data):
        user = self.context['request'].user

        validated_data['user'] = user

        address = Address.objects.create(**validated_data)

        return address


class AddressTitleSerializer(serializers.ModelSerializer):
    """
    地址标题
    """
    class Meta:
        model = Address
        fields = ('title',)


class UserDetailSerializer(serializers.ModelSerializer):
    """用户详细信息序列化器"""
    class Meta:
        model = User
        fields = ('id', 'username', 'mobile', 'email', 'email_active')

class EmailSerializer(serializers.ModelSerializer):
    """添加邮箱的更新序列化器"""
    class Meta:
        model = User
        fields = ('id','email')
        extra_kwargs = {
            'email':{
                'required':True
            }
        }



    def update(self, instance, validated_data):
        """重写为了在保存邮箱之前发送邮件"""

        instance.email = validated_data.get('email')
        instance.save()

        #生成url链接

        verify_url = instance.generate_email_verify_url()

        # 发送邮件(更新数据后返回数据前)
        send_verify_email.delay(instance.email,verify_url)

        return instance



class UserSerializer(serializers.ModelSerializer):
    """注册序列化器"""
    password2 = serializers.CharField(label='确认密码',write_only=True)
    allow = serializers.CharField(label='是否同意',write_only=True)
    sms_code = serializers.CharField(label='短信验证码',write_only=True)


    token = serializers.CharField(label='token口令',read_only=True)


    class Meta:
        model = User
        fields = ['id','username','mobile','password','password2','allow','sms_code','token']
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    # 追加验证手机号
    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$',value):
            raise serializers.ValidationError("手机号格式不正确")
        return value

    def validate_allow(self, value):
        if value != "true":
            raise serializers.ValidationError("请勾选用户协议")
        return value

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError("输入密码和确认密码不一致")
        # return attrs

    # def validator(self,attrs):
        redis_conn = get_redis_connection("verify_codes")
        mobile = attrs['mobile']
        sms_code = attrs['sms_code']
        sms_code_redis = redis_conn.get("sms_%s"%mobile)
        print(sms_code_redis)

        if sms_code is None:
            raise serializers.ValidationError("短信验证码无效")
        if sms_code != sms_code_redis.decode():
            raise serializers.ValidationError("短信验证码不正确")
        return attrs

    def create(self, validated_data):

        del validated_data['password2']
        del validated_data['sms_code']
        del validated_data['allow']

        #  保存注册用户数据到数据库
        user = User.objects.create(
            # username=validated_data['username'],
            # username = validated_data.get('username'),
            **validated_data
        )

        user.set_password(validated_data.get('password'))

        user.save()

        # 在注册成功后,响应之前,将JWT设置进去
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token

        return user



