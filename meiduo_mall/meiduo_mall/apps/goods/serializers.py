
from rest_framework import serializers

from .search_indexes import SKUIndex
from .models import SKU
from drf_haystack.serializers import HaystackSerializer


class SKUSerializer(serializers.ModelSerializer):
    """SKU商品序列化"""
    class Meta:
        model = SKU
        fields = ['id', 'name', 'price', 'default_image_url', 'comments']


class SKUSearchSerializer(HaystackSerializer):
    """队搜索的结果进行序列化"""

    object = SKUSerializer(read_only=True)

    class Meta:
        index_classes = [SKUIndex]
        fields = ['text','object']
