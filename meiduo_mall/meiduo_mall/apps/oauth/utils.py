from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadData
from django.conf import settings
from . import constants


def generate_save_user_token(openid):
    """
    对openid签名
    :param openid:
    :return:
    """
    # 创建序列化器对象
    s = Serializer(settings.SECRET_KEY,constants.save_qq_user_token_Expire)
    # 构造原始数据
    data = {'openid':openid}
    token = s.dumps(data).decode()
    return token

def check_save_user_token(access_token):
    """
    将密文openid转成明文openid
    :param access_token: 密文openid
    :return: 明文openid
    """
    # 创建序列化器对象
    s = Serializer(settings.SECRET_KEY, constants.save_qq_user_token_Expire)
    # 解码密文openid
    try:
        data = s.loads(access_token)
        openid = data.get('openid')
    except BadData:
        return None
    else:
        return openid


