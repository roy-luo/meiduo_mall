from django.conf.urls import url

from .views import OauthView,OauthUserView

urlpatterns = [
    url(r'^qq/authorization/$',OauthView.as_view()),
    url(r'^qq/user/$',OauthUserView.as_view()),

]