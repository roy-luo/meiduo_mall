from rest_framework import serializers

from .models import Area


class AreaSerializer(serializers.ModelSerializer):
    """省级数据序列化器"""

    class Meta:
        model = Area
        fields = ['id', 'name']


class SubAreaSerializer(serializers.ModelSerializer):
    """城市和区县的序列化器"""

    # 使用subs关联嵌套AreaSerializer实现子集的序列化器
    subs = AreaSerializer(many=True, read_only=True)

    class Meta:
        model = Area
        fields = ['id', 'name', 'subs']