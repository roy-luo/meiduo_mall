# 因为Django默认会将上传上来的文件存储到本地M目录下，
# 实际需求是需要存储到FastDFS服务器，所以需要自定义文件存储系统，修改存储的方案
from django.core.files.storage import Storage
from fdfs_client.client import Fdfs_client
from django.conf import settings


class FastDFSStorage(Storage):
    """自定义文件存储系统，修改存储的方案"""

    def __init__(self, client_conf=None, base_url=None):
        """
        构造方法，可以不带参数，也可以携带参数
        :param client_conf: client配置文件路径
        :param base_url: 文件读取服务器的IP
        """
        self.client_conf = client_conf or settings.FDFS_CLIENT_CONF
        self.base_url = base_url or settings.FDFS_BASE_URL


    def _open(self, name, mode='rb'):
        """
        储存类用于打开文件的实际工具
        :param name: 要打开的文件的名字
        :param mode: 打开文件方式
        :return: None
        """
        # 因为自动这个文件存储的类主要是做文件的存储，不会去打开文件，文档告诉我必须实现，但是又没用，所以pass
        pass


    def _save(self, name, content):
        """
        保存文件是被调用的:抓住这个时机，将上传到django的文件转存到FastDFS
        :param name: 要保存的文件名字
        :param content: 要保存的文件的内容，是File类型的对象，将来使用content.read()读取文件内容二进制
        :return: file_id,会自动的将file_id存储到模型类中对应的ImageField字段下
        file_id == group1/M00/00/00/wKhnh1uhqUCAAi89AAC4j90Tziw81.jpeg
        """
        # 对接FastDFS
        # client = Fdfs_client('meiduo_mall/utils/fastdfs/client.conf')
        client = Fdfs_client(self.client_conf)
        # ret = client.upload_by_filename('/Users/zhangjie/Desktop/01.jpeg')
        ret = client.upload_by_buffer(content.read())

        # 判断文件上传是否成功
        if ret.get('Status') != 'Upload successed.':
            raise Exception('FastDFS upload failed')

        # 如果上传成功读取file_id
        file_id = ret.get('Remote file_id')
        return file_id


    def exists(self, name):
        """
        判断文件在本地是否存储过
        :param name: 要判断的文件的名字
        :return: False，如果返回False表示本地没有，会去调用save()方法，在save()方法会去_save()方法
        """
        return False


    def url(self, name):
        """
        读取文件的路径的，必须读取全路径，交给<img src="">
        :param name: 要读取文件的名字,就是之前保存到数据库的file_id==group1/M00/00/00/wKhnh1uhqUCAAi89AAC4j90Tziw81.jpeg
        :return: http://192.168.103.135:8888/group1/M00/00/00/wKhnh1uhqUCAAi89AAC4j90Tziw81.jpeg
        """
        # return 'http://192.168.103.135:8888/' + name
        return self.base_url + name