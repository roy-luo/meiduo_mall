from django.db import models


class BaseModel(models.Model):
    """为了增加字段"""

    create_time = models.DateTimeField(verbose_name="创建时间",auto_now_add=True)

    update_time = models.DateTimeField(verbose_name="更新时间",auto_now=True)

    class Meta:
        abstract = True






