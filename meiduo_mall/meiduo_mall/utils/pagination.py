from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    """分页后端"""
    # 默认每页记录的条数，如果不传默认是2条
    page_size = 2
    # 自定义接受每页记录条数的字段名字
    page_size_query_param = 'page_size'
    # 默认么日夜记录最大的上限
    max_page_size = 20

    # 需求：用户要看第一页，每页2条记录
    # 测试地址：http://127.0.0.1:8000?page=1&page_size=3